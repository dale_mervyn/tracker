// ROUTES
carApp.config(function ($routeProvider) {
    $routeProvider
    
    .when('/', {
        templateUrl: 'templates/addService.html',
        controller: 'addServiceController'
    })
    
    .when('/viewServices', {
        templateUrl: 'templates/services.html',
        controller: 'servicesController'
    })
});