// CONTROLLERS
//We pass in an array that contains the function and dependancies because a minifier won't change the 
//name of things inside quotes. eg '$scope'
carApp.controller('addServiceController', ['$scope', '$resource', 'add_Service', 'getService', 'Car', function($scope, $resource, add_Service, getService, Car) {
   //Car defaults
    $scope.make = "";
    $scope.model = "";
    $scope.type = "Electric";
    $scope.year = 0;
    $scope.odometer = 0;
    $scope.selectedServices = [];
    
    var car = new Car($scope.make, $scope.model, $scope.type, $scope.year, $scope.odometer);
    var count = 0;
    
    //Car info needed to add a service
    $scope.make = add_Service.make;
    $scope.model = add_Service.model;
    $scope.type = add_Service.type;
    $scope.year = add_Service.year;
    $scope.odometer = add_Service.odometer;
    $scope.selectedServices = add_Service.selectedServices;
    
    $scope.$watch('make', function() {
        add_Service.make = $scope.make;
    });
    
    $scope.$watch('model', function() {
        add_Service.model = $scope.model;
    });
    
    $scope.$watch('type', function() {
        add_Service.type = $scope.type;
        
        var servicesCallback = function(response) {
            $scope.availableServices = response;
            console.info('trackerController callback: Available Services: ' + $scope.availableServices);
        }
    
    car.getAvailableServices(servicesCallback, $scope.type);
    });
    
    $scope.$watch('year', function() {
        add_Service.year = $scope.year;
    });
    
    $scope.$watch('odometer', function() {
        add_Service.odometer = $scope.odometer;
    });
    
    $scope.$watch('selectedServices', function() {
         add_Service.selectedServices = $scope.selectedServices;
        console.info('A service was selected! : ' + $scope.selectedServices);
    });
}]);

/**
* This controller will handle saving the car data.
* Will handle the update duties as well.
*/
carApp.controller('servicesController', ['$scope', '$resource', 'add_Service', function($scope, $resource, add_Service) {
    
    $scope.make = add_Service.make;
    $scope.model = add_Service.model;
    $scope.type = add_Service.type;
    $scope.year = add_Service.year;
    $scope.odometer = add_Service.odometer;
    $scope.selectedServices = add_Service.selectedServices;
    
    /**
    * Handle the submissions
    */
}]);