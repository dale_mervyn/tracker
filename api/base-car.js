// Define the main app and it's dependancies.                                                   
var carApp = angular.module('carApp', ['ngMessages','ngRoute','ngResource']);

// For Exception Handling
function CarException(message) {
  this.name = 'CarException';
  this.message= message;
}

CarException.prototype = new Error();
CarException.prototype.constructor = CarException;

//Defining our CAR factory
carApp.factory('BaseCar', ['$log', function($log) {
    /**
    * This Car data we can safely assume since it doesn't normally change.
    */
    var AVAILABLE_TYPES = ['Electric', 'Gas', 'Diesel', 'Hybrid'];
    
    var _make = '';
    var _model = '';
    var _type = '';
    var _year = '';
    var _odometer = 0;

    var _car_id = 0;
    
    //instantiate our car Object
    var BaseCar = function(make, model, type, year, odometer, car_id) {
        _make = make;
        _model = model;
        _type = type;
        _year = year;
        _odometer = odometer;
        
        _car_id = car_id;
        
        //Since this api is closely coupled to the database we can safely assume there are only the four types of car.
        if(AVAILABLE_TYPES.indexOf(type) < 0) {            
            throw new CarException('Car TYPE Error: Type Not Available.');
        }
        
        //Getters and Setters    
        this.getMake = function() {
            return _make;
        };    
        this.getModel = function() {
            return _model;
        };    
        this.getType = function() {
            return _type;
        };    
        this.getYear = function() {
            return _year;
        };    
        this.getOdometer = function() {
            return _odometer;
        };    
        this.getID = function() {
            return _car_id;
        };
    };
    
    return BaseCar;
}]);