carApp.factory('Car', ['$http', '$q', 'BaseCar', function ($http, $q, BaseCar) {
        
//    $scope.servicesQueryResult = angular.toJson($scope.servicesQueryResult, '\t');
    
    /**
    * This Car data is now coming from the database
    */
    var AVAILABLE_SERVICES = [];
    
    /**
    * Array that would hold the requested services for the car instance.
    * Also normally would be stored in the database.
    **/
    var _requestedServices = [];
    
    // This is the constructor that would be used.  The params are passed into the BaseCar's constructor.
    var Car = function (make, model, type, year, odometer) {
        BaseCar.apply(this, arguments);
        
        var self = this;
            
        /** 
        * Returns the available services array
        **/
        Car.prototype.getAvailableTypes = function (callback) {
            _getAvailableTypes(callback);
        };
        
        /** 
        * Returns the available services array
        **/
        Car.prototype.getAvailableServices = function (callback, injType) {
            _getAvailableServices(callback, injType);
        };
        
        /** 
        * Adds the service if it is available and not already included.
        * Returns 1 if successful.
        * Returns -1 if unsuccessful.
        **/
        Car.prototype.addRequestedService = function (service) {
            var success = -1;
            
            getAvailableServices();
                    
            if (AVAILABLE_SERVICES.indexOf(service) >= 0) {
                //Return 1 here since it is successful even 
                if (_requestedServices.indexOf(service) < 0) {
                    _requestedServices.push(service);
                    
                    success = 1;
                } else {
                    //Need to set success to one, since it is already added.
                    success = 1;
                }
            }

            return success;
        };
    
        /** 
        * Returns the requested services array.
        **/
        Car.prototype.getRequestedServices = function () {
            return _requestedServices;
        };
    
        /** 
        * Removes the service from the requested services list.
        *
        * Returns 1 if successful.
        * Returns -1 if unsuccessful.
        **/
        Car.prototype.removeRequestedService = function (service) {
            var index = _requestedServices.indexOf(service);
            var success = -1;
            
            if (index >= 0) {
                _requestedServices.splice(index, 1);
                
                success = 1;
            }
                        
            return success;
        };
                    
        var _getAvailableTypes = function (callback) {
			/**
			* Setting up to makes database calls
			*/
			var servPromise = $http.get("http://ec2-54-213-44-219.us-west-2.compute.amazonaws.com/SimplePHPExample/database/carServices.php");
            
            servPromise.then(function(response) {
                var services = response.data.Services;
                
                AVAILABLE_TYPES = [];
					
                for(var i = 0; i < services.length; i++){
                    AVAILABLE_TYPES.push(services[i].type);
                }
                
                console.log("REcieved a response from our server php. AVAILABLE_TYPES: " + AVAILABLE_TYPES);
                
                callback(AVAILABLE_TYPES);
            });
		}
                    
        var _getAvailableServices = function (callback, injType) {
            /**
            * Setting up to makes database calls
            */
            var servPromise = $http.get("http://ec2-54-213-44-219.us-west-2.compute.amazonaws.com/SimplePHPExample/database/carServices.php");

            servPromise.then(function(response) {
                var services = response.data.Services;
                
                var getCaseServices = function(type) {
                    for(var i = 0; i < services.length; i++) {
                        if(services[i].type === type){
                            delete services[i]['type'];
                            
                            //Services is an object.  Need to return an array.
                            return Object.keys(services[i]).map(function (key) {return services[i][key]});
                        }
                    }
                };
                
                switch(injType) {
                        case 'Electric':
                            //use types to get the services for that type.
                            AVAILABLE_SERVICES = getCaseServices("Electric");
                            break;
                        case 'Gas':
                            AVAILABLE_SERVICES = getCaseServices("Gas");
                            break;
                        case 'Diesel':
                            AVAILABLE_SERVICES = getCaseServices("Diesel");
                            break;
                        case 'Hybrid':
                            AVAILABLE_SERVICES = getCaseServices("Hybrid");
                            break;
                        default:
                            AVAILABLE_SERVICES = [];
                    }

                callback(AVAILABLE_SERVICES);
            });
        }
		
		var _getQuery = function () {
			return _queryServices();
		}
		
		var _getResultOfQueryServices = function () {
			
		}
    };
    
    // duplicate the original object prototype
    Car.prototype = new BaseCar('','','Electric',0,0,0);
    
    return Car;
}]);