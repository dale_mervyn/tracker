var injector = angular.injector(['ng', 'carApp']);

var init = {
    setup: function () {
        this.$scope = injector.get('$rootScope').$new();
    }
};

module('Base Car Tests', init);

//Test 1
test('1: it should create a BASE car successfully', function () {
    var BaseCar = injector.get('BaseCar');
    
    // BaseCar = function(make, model, type, year, odometer, car_id) {
    var testCar = new BaseCar('Toyota', 'Camry', 'Hybrid', 1997, 908, 1);
    
    equal(1, testCar.getID(), 'ID is 1');
    equal('Toyota', testCar.getMake(), 'Car Make is Toyota');
    equal('Camry', testCar.getModel(), 'Car Model is Camry');
    equal('Hybrid', testCar.getType(), 'Car Type is Hybrid');
    equal(1997, testCar.getYear(), 'Car Year is 1997');
    equal(908, testCar.getOdometer(), 'Car Odometer reading is 908');
});