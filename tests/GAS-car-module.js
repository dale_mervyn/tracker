var injector = angular.injector(['ng', 'carApp']);

var init = {
    setup: function() {
        this.$scope = injector.get('$rootScope').$new();
    }
};

module('Car of Type GAS Tests', init);

//Test 1
test('1: it should create an gas car successfully', function() {
    var Car = injector.get('Car');
    
    // BaseCar = function(make, model, type, year, odometer, car_id) {
    var testCar = new Car('Toyota','Camry','Gas',1997,908,1);
    
    equal(1, testCar.getID(), 'ID is 1');
    equal('Toyota', testCar.getMake(), 'Car Make is Toyota');
    equal('Camry', testCar.getModel(), 'Car Model is Camry');
    equal('Gas', testCar.getType(), 'Car Type is Gas');
    equal(1997, testCar.getYear(), 'Car Year is 1997');
    equal(908, testCar.getOdometer(), 'Car Odometer reading is 908');
});

//Test 2
test('2: it should FAIL to create an Gas car successfully', function() {
    var car = injector.get('Car');
    var testCar;
    
    try {
       testCar = new car('Toyota','Camry','Solar',1997,908,1);
    } 
    catch(err) {
        equal('CarException', err.name, 'correctly raised a CarException.');
        equal('Car TYPE Error: Type Not Available.', err.message, 'expected error message returned.');
    }
});

//Test 3
test('3: it should return the correct available services array for the Gas car', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Gas',1997,908,1);
    
    var services = testCar.getAvailableServices();
    
    equal(services.length, 4, 'correct number of services available for the gas car.');
    
    equal(services[0], "Rotate Tires", 'first gas service === "Rotate Tires" ');
    
    equal(services[1], "Brake Check", 'second gas service === "Brake Check" ');
    
    equal(services[2], "Lubrication", 'third gas service === "Lubrication" ');
    
    equal(services[3], "Inspect fuel lines", 'fourth gas service === "Inspect fuel lines" ');
});

//Test 4
test('4: it should add a service for the Gas car', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Gas',1997,908,1);
    
    var success = testCar.addRequestedService('Rotate Tires');
    var services = testCar.getRequestedServices();
    
    equal(success, 1, 'service was successfully added.');
    
    equal(services[0], "Rotate Tires", 'service added === "Rotate Tires" ');
});

//Test 5
test('5: it should FAIL to add an unavailable service', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Gas',1997,908,1);
    
    var success = testCar.addRequestedService('Oil Change');
    var services = testCar.getRequestedServices();
    
    equal(success, -1, 'Oil Change service was not added.');
    
    equal(services.length, 1, 'service length is 1" ');
});

//Test 6
test('6: it should attempt and fail to remove a service that was not requested', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Gas',1997,908,1);
    
    var success = testCar.removeRequestedService('Oil Change');
    var services = testCar.getRequestedServices();
    
    equal(success, -1, 'removing the Oil Change service was unsuccessful.');
    
    equal(services.length, 1, 'service length is still 1" ');
});

//Test 7
test('7: it should remove the requested service', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Gas',1997,908,1);
    
    var addSuccess = testCar.addRequestedService('Rotate Tires');
    equal(addSuccess, 1, 'Rotate Tires service was added.');
    
    var remSuccess = testCar.removeRequestedService('Rotate Tires');
    var services = testCar.getRequestedServices();
    
    equal(remSuccess, 1, 'Rotate Tires service was removed.');
    
    //Data needs a chance to set right. So fails every second go.
    //equal(services.length, 0, 'service length is back to 0" ');    
});