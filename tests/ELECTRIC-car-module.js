var injector = angular.injector(['ng', 'carApp']);

var init = {
    setup: function() {
        this.$scope = injector.get('$rootScope').$new();
    }
};

module('Car of Type ELECTRIC Tests', init);

//Test 1
test('1: it should create an electric car successfully', function() {
    var Car = injector.get('Car');
    
    // BaseCar = function(make, model, type, year, odometer, car_id) {
    var testCar = new Car('Toyota','Camry','Electric',1997,908,1);
    
    equal(1, testCar.getID(), 'ID is 1');
    equal('Toyota', testCar.getMake(), 'Car Make is Toyota');
    equal('Camry', testCar.getModel(), 'Car Model is Camry');
    equal('Electric', testCar.getType(), 'Car Type is Electric');
    equal(1997, testCar.getYear(), 'Car Year is 1997');
    equal(908, testCar.getOdometer(), 'Car Odometer reading is 908');
});

//Test 2
test('2: it should FAIL to create an Electric car successfully', function() {
    var car = injector.get('Car');
    var testCar;
    
    try {
       testCar = new car('Toyota','Camry','Solar',1997,908,1);
    } 
    catch(err) {
        equal('CarException', err.name, 'correctly raised a CarException.');
        equal('Car TYPE Error: Type Not Available.', err.message, 'expected error message returned.');
    }
});

//Test 3
test('3: it should return the correct available services array for the Electric car', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Electric',1997,908,1);
    
    var services = testCar.getAvailableServices();
    
    var done = assert.async();
    var input = $( testCar ).focus();
    
    setTimeout(function() {
        equal( document.activeElement, input[0], "Input was focused" );
        done();
    });
    var servicesCallback = function(response) {
        'Rotate Tires', 'Battery Check', 'Lubrication', 'Sytems Check'
        equal(services.length, 4, 'correct number of services available for the gas car.');

        equal(services[0], "Rotate Tires", 'first electric service === "Rotate Tires" ');

        equal(services[1], "Battery Check", 'second electric service === "Battery Check" ');

        equal(services[2], "Lubrication", 'third electric service === "Lubrication" ');

        equal(services[3], "Sytems Check", 'fourth electric service === "Sytems Check" ');
    }
    
    testCar.getAvailableServices(servicesCallback, 'Electric');
    
    
    equal('', '', '');
});

QUnit.asyncTest('max', function (assert) {
    expect(4);
    QUnit.stop(3);
    
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Electric',1997,908,1);

    window.setTimeout(function() {
        testCar.getAvailableServices(servicesCallback, 'Electric');
        equal(services.length, 4, 'correct number of services available for the Electric car.');
        QUnit.start();
    }, 0);

   window.setTimeout(function() {
      assert.strictEqual(max(3, 1, 2), 3, 'All positive numbers');
      QUnit.start();
   }, 0);

   window.setTimeout(function() {
      assert.strictEqual(max(-10, 5, 3, 99), 99, 'Positive and negative numbers');
      QUnit.start();
   }, 0);

   window.setTimeout(function() {
      assert.strictEqual(max(-14, -22, -5), -5, 'All positive numbers');
      QUnit.start();
   }, 0);   
});

//Test 4
test('4: it should add a service for the Electric car', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Electric',1997,908,1);
    
    var success = testCar.addRequestedService('Sytems Check');
    var services = testCar.getRequestedServices();
    
    equal(success, 1, 'service was successfully added.');
    
    equal(services[0], "Sytems Check", 'service added === "Sytems Check" ');
});

//Test 5
test('5: it should FAIL to add an unavailable service', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Electric',1997,908,1);
    
    var success = testCar.addRequestedService('Igniter Check');
    var services = testCar.getRequestedServices();
    
    equal(success, -1, 'Igniter Check service was not added.');
    
    equal(services.length, 1, 'service length is 1" ');
});

//Test 6
test('6: it should attempt and fail to remove a service that was not requested', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Electric',1997,908,1);
    
    var success = testCar.removeRequestedService('Lubrication');
    var services = testCar.getRequestedServices();
    
    equal(success, -1, 'removing the Lubrication service was unsuccessful.');
    
    equal(services.length, 1, 'service length is still 1" ');
});

//Test 7
test('7: it should remove the requested service', function() {
    var car = injector.get('Car');
    
    var testCar = new car('Toyota','Camry','Electric',1997,908,1);
    
    var addSuccess = testCar.addRequestedService('Rotate Tires');
    equal(addSuccess, 1, 'Rotate Tires service was added.');
    
    var remSuccess = testCar.removeRequestedService('Rotate Tires');
    var services = testCar.getRequestedServices();
    
    equal(remSuccess, 1, 'Rotate Tires service was removed.');
    
    equal(services.length, 0, 'service length is back to 0" ');
});