//SERVICES
carApp.service('add_Service', function () {
    this.make = "Nissan";
    this.model = "Juke";
    this.type = "Electric";
    this.year = "2007";
    this.odometer = 67890;
    this.selectedServices = [];
});